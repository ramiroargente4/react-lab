﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cambiando_elemento : MonoBehaviour
{
    //public GameObject microscopio;
    //public GameObject kitasato;
    //public int contador;

    public GameObject[] objetos_botones;
    public string[] nombres_objetos;

    public Vector3 posicion_inicial;

    public void turning_off()
    {
        objetos_botones[0].gameObject.SetActive(false);
        objetos_botones[1].gameObject.SetActive(false);
        objetos_botones[2].gameObject.SetActive(false);
        objetos_botones[3].gameObject.SetActive(false);
        objetos_botones[4].gameObject.SetActive(false);
        objetos_botones[5].gameObject.SetActive(false);
        objetos_botones[6].gameObject.SetActive(false);
        objetos_botones[7].gameObject.SetActive(false);
        objetos_botones[8].gameObject.SetActive(false);
    }

    public void cambia_balon()
    {
        turning_off();
        objetos_botones[1].gameObject.SetActive(true);
        //objetos_botones[1].x = posicion_inicial.x;
    }

    public void cambia_microscopio()
    {
        turning_off();
        objetos_botones[0].gameObject.SetActive(true);
    }
    public void cambia_Bunsen()
    {
        turning_off();
        objetos_botones[2].gameObject.SetActive(true);
    }
    public void cambia_tela_metalica()
    {
        turning_off();
        objetos_botones[3].gameObject.SetActive(true);
    }
    public void cambia_crisol()
    {
        turning_off();
        objetos_botones[4].gameObject.SetActive(true);
    }
    public void cambia_bureta()
    {
        turning_off();
        objetos_botones[5].gameObject.SetActive(true);
    }
    public void cambia_trianguloDeArsilla()
    {
        turning_off();
        objetos_botones[6].gameObject.SetActive(true);
    }
    public void cambia_tripode()
    {
        turning_off();
        objetos_botones[7].gameObject.SetActive(true);
    }
    public void cambia_cuentagotas()
    {
        turning_off();
        objetos_botones[8].gameObject.SetActive(true);
    }
}
