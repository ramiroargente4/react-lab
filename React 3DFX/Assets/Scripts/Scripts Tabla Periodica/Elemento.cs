using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Elemento : MonoBehaviour
{

    public ElementoData data;
    public Config_Data config;
    
    public Text txtNro;
    public Text txtMasa;
    public Text txtSimbolo;
    public Text txtNombre;

    Button button;
    ColorBlock colors;

    public enum TipoElemento{ metalesAlcalinos, alcalinoterreos, metales, metalesTransicion,lantanidos, metaloides, nometales, halogenos, gasesNobles, actinidos, noposee}

    public TipoElemento tipoElemento;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        SetConfig();
    }

    void SetConfig(){
        
        SetColors();
        button.colors = colors;
        txtNro.text = data.nroAtomico.ToString();
        txtMasa.text = data.masaAtomica.ToString();  
        txtSimbolo.text = data.simbolo.ToString();
        txtNombre.text = data.nombre.ToString();
    }

    void SetColors(){

        colors.disabledColor = config.disabled;
        colors.fadeDuration = 0.1f;
        colors.highlightedColor = config.highlighted;
        colors.normalColor = config.normal;
        colors.pressedColor = config.pressed;
        colors.selectedColor = config.selected;
        colors.colorMultiplier = 1;

         switch(tipoElemento)
        {
            case TipoElemento.actinidos:
                colors.normalColor = config.actinidos;
                break;
            case TipoElemento.alcalinoterreos:
                colors.normalColor = config.alcalinoterreos;
                break;
            case TipoElemento.gasesNobles:
                colors.normalColor = config.gasesNobles;
                break;
            case TipoElemento.halogenos:
                colors.normalColor = config.halogenos;
                break;
            case TipoElemento.lantanidos:
                colors.normalColor = config.lantanidos;
                break;
            case TipoElemento.metales:
                colors.normalColor = config.metales;
                break;
            case TipoElemento.metalesAlcalinos:
                colors.normalColor = config.metalesAlcalinos;
                break;
            case TipoElemento.metalesTransicion:
                colors.normalColor = config.metalesTransicion;
                break;
            case TipoElemento.metaloides:
                colors.normalColor = config.metaloides;
                break;
            case TipoElemento.nometales:
                colors.normalColor = config.noMetales;
                break;
            case TipoElemento.noposee:
                colors.normalColor = config.noPosee;
                break;
        }
        
    }
}
