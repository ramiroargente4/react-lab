using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ElementoData", menuName = "AgregarElementos/Elemento Data")]
public class ElementoData : ScriptableObject
{

public int nroAtomico;
public float masaAtomica;
public string simbolo;
public string nombre;
}
