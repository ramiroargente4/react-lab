
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Config_Data", menuName = "AgregarElementos/Configuration Data")]
public class Config_Data : ScriptableObject
{
    public Color metalesAlcalinos;
    public Color alcalinoterreos;
    public Color metales;
    public Color metalesTransicion;
    public Color lantanidos;
    public Color metaloides;
    public Color noMetales;
    public Color halogenos;
    public Color gasesNobles;
    public Color actinidos;
    public Color noPosee;

    public Color normal;
    public Color highlighted;
    public Color pressed;
    public Color selected;
    public Color disabled;

}
