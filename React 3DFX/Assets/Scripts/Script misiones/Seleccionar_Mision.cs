﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Seleccionar_Mision : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
            {
                if (Input.GetMouseButtonDown(0))
                if (hit.transform.tag == "EscenarioTabla") 
                {
                    SceneManager.LoadScene("labcopia");
                }
            if (Input.GetMouseButtonDown(0))
                if (hit.transform.tag == "volverAlab")
                {
                    SceneManager.LoadScene("Lab");
                }
            if (Input.GetMouseButtonDown(0))
                if (hit.transform.tag == "IrInvernadero")
                {
                    SceneManager.LoadScene("Invernadero");
                }
            if (Input.GetMouseButtonDown(0))
                if (hit.transform.tag == "IrLabViejo")
                {
                    SceneManager.LoadScene("labviejo");
                }
        }
    }
}
