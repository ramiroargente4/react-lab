﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class mision : MonoBehaviour
{
    public GameObject actividad;
    public Text consigna;
    ElementalInformation funcion;
    RayCastScript movimiento;

    int comprobar;


    // Start is called before the first frame update
    void Start()
    {
        funcion = GameObject.FindGameObjectWithTag("tablaexplicada").GetComponent<ElementalInformation>();

        movimiento = FindObjectOfType<RayCastScript>();
        comprobar = movimiento.comprobante;
    }

    // Update is called once per frame
    void Update()
    {
        comprobar = 0;

         RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
            {
                if (Input.GetMouseButtonDown(0))
                if (hit.transform.tag == "tablaexplicada")
                {
                    consigna.text = "Estas viendo la explicacion de los elementos, prueba a interactuar con alguno.";
                }
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                actividad.SetActive(false);
            }

    }
}