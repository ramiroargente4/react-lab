﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class variables : MonoBehaviour
{
 
    public GameObject texto;
    public GameObject titulo;
    public GameObject camaraJuego;
    public GameObject camaraDescripcion;
    public GameObject cartelAviso;
    public GameObject cursor;
    public GameObject informacionFrasco;
    public GameObject click;
    public GameObject imagenclick;
    public GameObject imagenclicktabla;
    public GameObject informacionfrascocanvas;
    public GameObject canvastabla;
    public GameObject imagenP;
    public GameObject parasalir;
    public GameObject tablacomun;
    public GameObject tablaextendida;
    public GameObject colorescomun;
    public GameObject coloresextendida;
    public GameObject invernadero;
    public Text aviso;
    public Text avisoClick;
    string nombre;
    string nombreFrasco;
    ElementalInformation funcion;
    public int comprobante;
    [SerializeField] private Animator anim;
    public GameObject jugador;
    public GameObject mov;
    public GameObject camarajugador;
    public Camera cameraComponent;

    public MovLerp trato;

    public RayCastScript ray;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController movimiento;
}
