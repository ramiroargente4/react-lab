﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetecccionColisiones : MonoBehaviour
{
    InstanciaCajas instanciaCajasScript;
    GameManager GM;
    public Material liquidofrasco;

    // Start is called before the first frame update
    void Start()
    {
        instanciaCajasScript = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<InstanciaCajas>();
        GM = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision tocado)
    {
        if (tocado.gameObject.tag == "Quimico")
        {
            liquidofrasco.color = Color.black;
            GM.gameOver = true;
        }
        instanciaCajasScript.ClonaCajas();
        Destroy(gameObject);
    }

}
