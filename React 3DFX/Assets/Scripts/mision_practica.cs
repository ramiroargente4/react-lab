﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mision_practica : MonoBehaviour
{
    public GameObject pregunta;
    public GameObject iniciar;
    public GameObject opciones;
    public GameObject objetosexperimento; //NOTA, MODIFICAR OBJETOSEXPERIMENTO CON UN GRUPO DE COSAS QUE SEA RELEVANTE A LA TRIVIA.
    public Text consigna;
    public GameObject avisodconsigna;
    [SerializeField] private bool pregunta_respondida = false;
    public GameObject correcta;
    public GameObject H14Na23_incorrecta;
    public GameObject NaNO_incorrecta;

    public InstanciaCajas anim;

    public bool activarsodio;
    public Animator animSodio;

    void seleccion_opcion (RaycastHit hit)
    {
        ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
        if (info.nombre == "NaNO" && hit.transform.tag == "Libro")
        {
            correcta.SetActive(false);
            H14Na23_incorrecta.SetActive(false);
            NaNO_incorrecta.SetActive(true);

            pregunta_respondida = true;
        }
        else if (info.nombre == "H14Na23" && hit.transform.tag == "Libro")
        {
            NaNO_incorrecta.SetActive(false);
            correcta.SetActive(false);
            H14Na23_incorrecta.SetActive(true);

            pregunta_respondida = true;
        }
        else if (info.nombre == "NaOH" && hit.transform.tag == "Libro")
        {
            H14Na23_incorrecta.SetActive(false);
            NaNO_incorrecta.SetActive(false);
            correcta.SetActive(true);

            pregunta_respondida = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        pregunta.SetActive(false);
        opciones.SetActive(false);
        objetosexperimento.SetActive(false);
        iniciar.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (activarsodio == true)
        {
            animSodio.enabled = !animSodio.enabled;
        }
        if (pregunta_respondida == true)
        {
            //consigna.text = "Inicia el experimento, y compara lo que ves con tu prediccion inicial.";       
        }
        RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
            {
                if (Input.GetMouseButtonDown(0))           
                if (hit.transform.tag == "Libro")
                {                  
                    iniciar.SetActive(false);
                    pregunta.SetActive(true);
                    opciones.SetActive(true);
                    objetosexperimento.SetActive(true);
                    avisodconsigna.SetActive(false);
                    consigna.text = "Lee la trivia, intenta predecir lo que pasara seleccionando una opcion, e inicia el experimento para comprobar.";
                    seleccion_opcion(hit);
                }
                if (Input.GetMouseButtonDown(0))
                if (hit.transform.tag == "iniciarSodio" && pregunta_respondida == true)
                {
                    activarsodio = true;
                    consigna.text = "Sigue explorando el contenido del laboratorio libremente o elige un escenario en el pizarron.";
                    avisodconsigna.SetActive(true);
                }
        }       
    }
}
