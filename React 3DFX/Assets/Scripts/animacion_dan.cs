﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animacion_dan : MonoBehaviour
{
    public GameObject endPosition;
    private Vector3 startPosition;
    private float desiredDuration = 3.5f; //entre mas grande el numero mas lento va
    private float elapsedTime;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        float percentageComplete = elapsedTime / desiredDuration;

        transform.position = Vector3.Lerp(startPosition, endPosition.transform.position, percentageComplete);
        
    }
}
