﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mision_elementos : MonoBehaviour
{

    [SerializeField] private int contador = 0;
    bool cloro_terminado = false;
    bool sodio_terminado = false;
    bool argon_terminado = false;
    public bool elementos_terminados = false;
    bool tabla_periodica = false;

    public Text descripcion;
    public Text descripcion2;
    public Text actividad;
    public Text titular;
    public Image imagen_referencia;
    public GameObject imagen_P;
    public GameObject imagen_lupa;
    public GameObject contenedor; 
    public GameObject otroMas;
    public GameObject panel;
    public GameObject cursor;


    public void mision(RaycastHit hit) 
    {
        if (hit.transform.tag == "Numero atomico" && contador == 0)
        {
            ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
            contenedor.SetActive(true);
            otroMas.SetActive(true);
            imagen_P.SetActive(true);
            imagen_lupa.SetActive(true);
            //panel.SetActive(false);
            descripcion.text = info.frascoDescripcion;
            descripcion2.text = info.frascoDescripcion2;
            titular.text = info.frascoTitulo;
            imagen_referencia.sprite = info.imagenFrasco;
            //actividad.text = "Andan a la Electronegatividad de " + info.nombre;
            actividad.text = "Dirigase hacia el Simbolo de " + info.nombre + " y despliegue la descripcion de la misma manera." ;
            contador++;
        }
        else if (hit.transform.tag == "Simbolo" && contador == 1)
        {
            ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
            contenedor.SetActive(true);
            otroMas.SetActive(true);
            descripcion.text = info.frascoDescripcion;
            descripcion2.text = info.frascoDescripcion2;
            titular.text = info.frascoTitulo;
            imagen_referencia.sprite = info.imagenFrasco;
            if (info.nombre == "Sodio")
            {
                actividad.text = "Ve al proximo elemento.";
            }
            else if (info.nombre == "Argon")
            {
                actividad.text = "Presiona el interruptor.";
            }
            else 
            {
            actividad.text = "Bien, ahora dirigase hacia el Nombre del elemento de " + info.nombre + " y repita la accion.";
            }
            contador++;
        }
        else if (hit.transform.tag == "Nombre del elemento" && contador == 2)
        {
            ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
            contenedor.SetActive(true);
            otroMas.SetActive(true);
            descripcion.text = info.frascoDescripcion;
            descripcion2.text = info.frascoDescripcion2;
            titular.text = info.frascoTitulo;
            imagen_referencia.sprite = info.imagenFrasco;
            actividad.text = "Investigue el Peso atomico de " + info.nombre + ".";
            contador++;
        }
        else if (hit.transform.tag == "Peso atomico" && contador == 3)
        {
            ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
            contenedor.SetActive(true);
            otroMas.SetActive(true);
            descripcion.text = info.frascoDescripcion;
            descripcion2.text = info.frascoDescripcion2;
            titular.text = info.frascoTitulo;
            imagen_referencia.sprite = info.imagenFrasco;
            actividad.text = "Investigue la Electronegatividad de " + info.nombre + ".";
            contador++;
        }
        else if (hit.transform.tag == "Electronegatividad" && contador == 4)
        {
            ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
            contenedor.SetActive(true);
            otroMas.SetActive(true);
            descripcion.text = info.frascoDescripcion;
            descripcion2.text = info.frascoDescripcion2;
            titular.text = info.frascoTitulo;
            imagen_referencia.sprite = info.imagenFrasco;
            actividad.text = "Investigue cuales son los Electrones de valencia de " + info.nombre;
            contador++;
        }
        else if (hit.transform.tag == "Electrones de valencia" && contador == 5)
        {
            ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
            contenedor.SetActive(true);
            otroMas.SetActive(true);
            descripcion.text = info.frascoDescripcion;
            descripcion2.text = info.frascoDescripcion2;
            titular.text = info.frascoTitulo;
            imagen_referencia.sprite = info.imagenFrasco;
            if (info.nombre == "Argon")
            {
                actividad.text = "Presiona el interruptor para cambiar el modo de la tabla periodica en la pared.";
            }
            else 
            {
            actividad.text = "Ve al proximo elemento";
            }
            contador++;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        contenedor.SetActive(false);
        otroMas.SetActive(false);
        actividad.text = "Apoye el cursor sobre el titulo del pizarron, Tabla periodica de elementos, y presione click.";
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
        {
            ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();

            cursor.SetActive(true);
            if (Input.GetMouseButtonDown(0))
            if (info.nombre == "Tabla periodica" && cloro_terminado == false)
            {
                tabla_periodica = true;
                actividad.text = "Ve al elemento Cloro e investigue su numero atomico.";
            }
            if (info.nombre == "Cloro" && cloro_terminado == false && tabla_periodica == true)
            {

                if (Input.GetMouseButtonDown(0))
                    mision(hit);
                if (contador == 6)
            {
                    cloro_terminado = true;
            }

            }


            else if (info.nombre == "Sodio" && cloro_terminado == true && sodio_terminado == false)
            {   
                if (contador == 6)
                {
                    contador = 4;
                }
                if (Input.GetMouseButtonDown(0))

                    mision(hit);

                if (contador == 6)
                {
                    sodio_terminado = true;
                }
            }

            else if (info.nombre == "Argon" && sodio_terminado == true && argon_terminado == false) 
            {
                if (contador == 6)
                {
                    contador = 4;
                }

                if (Input.GetMouseButtonDown(0))

                    mision(hit);

                if ( contador == 6)
                {
                    argon_terminado = true;
                }
            }



           else if (cloro_terminado && sodio_terminado && argon_terminado)
            {
                //caca.interruptor_tablaperidioca(hit);
                //funcion.interruptor_tablaperidioca(hit);
                elementos_terminados = true;
            }
        }
    }


}
