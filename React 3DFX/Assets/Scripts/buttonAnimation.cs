﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonAnimation : MonoBehaviour
{
    [SerializeField] Animator anim;
    [SerializeField] string boolName;

    public void OnMouseExit() {
        anim.SetBool(boolName, false);
    }
    public void OnMouseEnter() {
        anim.SetBool(boolName, true);
    }
}
