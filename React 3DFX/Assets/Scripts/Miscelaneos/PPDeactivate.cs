﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PPDeactivate : MonoBehaviour
{
    public bool PostProcessing;
    public GameObject PostFX;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("PP") == 0) 
        {
            PostFX.SetActive(false);
        }
        else
        {
            PostFX.SetActive(true);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
