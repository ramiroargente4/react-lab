﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CargarPorTiempoEscena : MonoBehaviour
{
    [SerializeField]
    private float tardarantesdecargar = 1f;
    [SerializeField]
    private string escenaacargar;

    public GameObject canvastabla;
    public Camera pantallaCarga;
    public GameObject pantallaInfo;

    void Start()
    {
        pantallaCarga.enabled = false;
        
    }

    private void Update()

    {
        //if (Input.GetKeyDown(KeyCode.Space)){
        //    StartCoroutine(Esperar());
        //}
    }

    IEnumerator Esperar()
    {
        pantallaCarga.enabled = true;
        yield return new WaitForSeconds(tardarantesdecargar);
        pantallaCarga.enabled = false;
        pantallaInfo.SetActive(true);
        Debug.Log("Cambia la cámara");
        canvastabla.SetActive(true);
    }

    public void CambiarCamara()
    {
        StartCoroutine(Esperar());
    }
}
