﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class mision_segundaparte : MonoBehaviour
{

    public GameObject contenedor; 
    public Text descripcion;
    public Text descripcion2;
    public Text actividad;
    public Text titular;
    public Image imagen_referencia;
    public GameObject otroMas;

    public MovLerp trato;
    public Text acto; 

    [SerializeField] private int contador = 0;
    [SerializeField] private bool tipo_elemento = false;

    public void alcalinos_mision(RaycastHit hit)
    {
        ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
        contenedor.SetActive(true);
        otroMas.SetActive(true);
        descripcion.text = info.frascoDescripcion;
        descripcion2.text = info.frascoDescripcion2;
        titular.text = info.frascoTitulo;
        imagen_referencia.sprite = info.imagenFrasco;
        //tipo_elemento = true;
        contador++;
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
        {
            ElementalInformation info = hit.collider.gameObject.GetComponent<ElementalInformation>();
            if (Input.GetMouseButtonDown(0))
            if (info.nombre == "Sodio" && hit.transform.tag == "TablaPeriodica")
            {
                acto.text = "Ve a otros no metales";
            }
            else if (info.nombre == "Cloro" && hit.transform.tag == "TablaPeriodica")
            {
                acto.text = "Ve a gases nobles";
            }
            else if (info.nombre == "Argon" && hit.transform.tag == "TablaPeriodica")
            {
                acto.text = "Terminaste la explicacion, felicitaciones";
            }
            if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etAlcalino")
            {
                if (contador == 0 && info.nombre == "Alcalinos")
                {
                alcalinos_mision(hit);
                acto.text = "Ve y accede a Sodio";
                }
            }
            else if (hit.transform.tag == "etOtrosnometales")
            {
                if (contador == 1 && info.nombre == "Otros no metales")
                {
                alcalinos_mision(hit);
                acto.text = "Ve y accede a Cloro";
                }
            }
            else if (hit.transform.tag == "etGasesnobles")
            {
                if (contador == 2 && info.nombre == "Gases nobles")
                {
                alcalinos_mision(hit);
                acto.text = "Ve y accede a Argon";
                }
            }

        }
        
    }
}
