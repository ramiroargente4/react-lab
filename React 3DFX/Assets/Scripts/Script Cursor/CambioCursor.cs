﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CambioCursor : MonoBehaviour
{

    public RawImage cursor;
    //public Color textureDefault = Color.white;
    //public Color textureInteraccion = Color.red;
    public Texture textureDefault;
    public Texture textureInteraccion;
    public Texture textureInteraccionFrasco;
    public Texture textureInterracionBoton;

    public float timer = 10;

    // Start is called before the first frame update
    void Start()
    {
        //cursor.texture = textureDefault;
        cursor.GetComponent<RawImage>().color = new Color32(255, 255, 255, 225);

    }

    public void temporizador()
    {
        cursor.GetComponent<RawImage>().color = new Color32(41, 192, 13, 255);
        while (timer > 0)
                {
                timer = timer - 1 * Time.deltaTime;
                }
                if (timer <= 0)
                {
                    cursor.GetComponent<RawImage>().color = new Color32(41, 192, 13, 255);
                }
    }


    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
        {
            if (hit.transform.tag == "TablaPeriodica" || hit.transform.tag == "Objetivo" || hit.transform.tag == "etAlcalino" || hit.transform.tag == "etAlcalinoterreos" || hit.transform.tag == "etLantanidos" || hit.transform.tag == "etActinido" || hit.transform.tag == "etMetaldetransicion" || hit.transform.tag == "etMetalbloquep" || hit.transform.tag == "etSemimetal" || hit.transform.tag == "etOtrosnometales" || hit.transform.tag == "etGasesnobles" || hit.transform.tag == "etMetal" || hit.transform.tag == "etNometal" || hit.transform.tag == "etReiniciarTabla" || hit.transform.tag == "Nombre del elemento" || hit.transform.tag == "Electronegatividad" || hit.transform.tag =="Peso atomico" || hit.transform.tag == "Electrones de valencia" || hit.transform.tag == "Simbolo" || hit.transform.tag == "Numero atomico" || hit.transform.tag == "Elemento mision" || hit.transform.tag =="Iniciar#1" || hit.transform.tag == "Iniciar#2" || hit.transform.tag == "iniciarSodio")
            {
                //cursor.texture = textureInteraccion;
                cursor.GetComponent<RawImage>().color = new Color32(0, 12, 43, 255);
                if (Input.GetMouseButton(0))
                temporizador();
            }
            else if (hit.transform.tag == "Elementos de laboratorio" || hit.transform.tag == "Agarrable" || hit.transform.tag == "AbrirUwU" || hit.transform.tag == "Consigna 1" || hit.transform.tag == "Consigna 2" || hit.transform.tag == "Consigna 3" || hit.transform.tag == "Libro" || hit.transform.tag == "VideoReaccion" || hit.transform.tag == "accedervisor")
            {
                //cursor.texture = textureInteraccionFrasco;
                cursor.GetComponent<RawImage>().color = new Color32(93, 189, 178, 255);
                if (Input.GetMouseButton(0))
                temporizador();
                
            }
            else if (hit.transform.tag == "invernadero" || hit.transform.tag == "invernadero 2" || hit.transform.tag == "tablacomun" || hit.transform.tag == "tablaextendida" || hit.transform.tag == "puertasalir" || hit.transform.tag == "lucesprender" || hit.transform.tag == "lucesapagar" || hit.transform.tag == "tablaagrandada" || hit.transform.tag == "tablaexplicada" || hit.transform.tag == "EscenarioTabla" || hit.transform.tag == "volverAlab" || hit.transform.tag == "IrInvernadero" || hit.transform.tag == "IrLabViejo")
            {
                //cursor.texture = textureInterracionBoton;
                cursor.GetComponent<RawImage>().color = new Color32(93, 189, 178, 255);
                if (Input.GetMouseButton(0))
                temporizador();
            }
            else
            {
                //cursor.texture = textureDefault;
                cursor.GetComponent<RawImage>().color = new Color32(255, 255, 255, 225);
                timer = 10;
                 // 41 192 13 255
            }


        }


    }
}