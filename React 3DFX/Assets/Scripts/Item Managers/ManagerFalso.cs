﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerFalso : MonoBehaviour
{
    //public Text frasco;
    public Text frascoDescripcion;
    public Text frascoDescripcion2;
    public Image imagenFrasco;
    public Text frascoNombre;
    public int cantidadFrasco;
    public Text frascoTitulo;


    public void DarInfo(string frascoDescripcionInfo, string frascoDescripcion2Info, Sprite imagenFrascoInfo, string frascoNombreInfo, int cantidadFrascoInfo, string frascoTituloInfo)
    {
        //frasco.text = frascoInfo;
        frascoDescripcion.text = frascoDescripcionInfo;
        frascoDescripcion2.text = frascoDescripcion2Info;
        imagenFrasco.sprite = imagenFrascoInfo;
        frascoNombre.text = frascoNombreInfo;
        cantidadFrasco = cantidadFrascoInfo;
        frascoTitulo.text = frascoTituloInfo;
    }

}