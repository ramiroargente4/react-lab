﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElementalInformation : MonoBehaviour
{
    //TABLA PERIODICA
    public string nombre;
    public string descripcion;
    public string descripcionp2;
    [TextArea(3, 10)]
    public string numeroTablaPeriodica;
    public string PaQueSirve;
    public string electronValencia;
    public Sprite imagenElemento;
    //ELEMENTOS
    public string frascoDescripcion;
    public string frascoDescripcion2;
    public Sprite imagenFrasco;
    public string frascoNombre;
    public string frascoTitulo;
    public int cant_frasco;


    public EmManager manager;
    public ManagerFalso managerFalso;


    public void giveInformation()
    {
        manager.DarInformacion(nombre, descripcion, descripcionp2, numeroTablaPeriodica, PaQueSirve, electronValencia, imagenElemento);
    }


    public void dameLaInfo()
    {
        managerFalso.DarInfo(frascoDescripcion, frascoDescripcion2, imagenFrasco, frascoNombre,cant_frasco, frascoTitulo);
    }
}