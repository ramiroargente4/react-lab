﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class EmManager : MonoBehaviour
{
    public Text nombre;
    public Text descripcion;
    public Text descripcionp2;
    public Text numeroTablaPeriodico;
    public Text paQueSirve;
    public Text electronValencia;
    public Image imagenElemento;

    public void DarInformacion(string nombreInfo, string descripcionInfo, string descripcionInfo2, string numeroTabla, string paQueSirveInfo, string electronValenciaInfo, Sprite imagenElementoInfo)
    {
        nombre.text = nombreInfo;
        descripcion.text = descripcionInfo;
        descripcionp2.text = descripcionInfo2;
        numeroTablaPeriodico.text = numeroTabla;
        paQueSirve.text = paQueSirveInfo;
        electronValencia.text = electronValenciaInfo;
        imagenElemento.sprite = imagenElementoInfo;
    }

}
