﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarre : MonoBehaviour
{
    PhysicsFollow a;
    public GameObject posicionComienzo;
    public GameObject puntoPaAgarrar;
    private GameObject pickedObject = null;
    public Quaternion ubicacion;



    //public float posX;
    //public float posY;
    //public float posZ;
    //public Vector3 posicion;

    // Start is called before the first frame update
    void Start()
    {
        puntoPaAgarrar.transform.position = posicionComienzo.transform.position;
        puntoPaAgarrar.transform.rotation = posicionComienzo.transform.rotation;

    }

    // Update is called once per frame
    void Update()
    {
        if (pickedObject != null)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {

                Destroy(a);
                pickedObject.GetComponent<Rigidbody>().useGravity = true;

                pickedObject.GetComponent<Rigidbody>().isKinematic = false;

                pickedObject.transform.SetParent(null);

                //pickedObject.transform.position = posicion;

                pickedObject = null;

                puntoPaAgarrar.transform.position = posicionComienzo.transform.position;
                puntoPaAgarrar.transform.rotation = posicionComienzo.transform.rotation;

            }

        }

        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        Debug.DrawRay(transform.position, forward, Color.green);


        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
        {
            if (hit.transform.tag == "Agarrable")
            {
                if (Input.GetKeyDown(KeyCode.F) && pickedObject == null)
                {
                    //puntoPaAgarrar.transform.position = posicionComienzo.transform.position;
                    //puntoPaAgarrar.transform.rotation = posicionComienzo.transform.rotation;

                    GameObject Go = hit.collider.gameObject;

                    //Go.GetComponent<Rigidbody>().useGravity = false;

                    //Go.GetComponent<Rigidbody>().isKinematic = true;

                    Go.transform.SetParent(puntoPaAgarrar.gameObject.transform);

                    //Go.transform.localPosition = Vector3.zero;

                    pickedObject = hit.collider.gameObject;
                    
                    a = pickedObject.AddComponent<PhysicsFollow>();
                    a.targetTransform = puntoPaAgarrar.transform;
                    //pickedObject.transform.rotation = ubicacion;
                }
            }
        }

    }
}