﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestartBarato : MonoBehaviour
{

    public GameObject camarajugador;
    public Camera papa_frita;
    public GameObject mov;
    public GameObject jugador;

    public GameObject texto;
    public GameObject titulo;
    public GameObject camaraJuego;
    public GameObject camaraDescripcion;
    public GameObject cartelAviso;
    public GameObject cursor;
    public GameObject informacionFrasco;
    public GameObject click;
    public GameObject imagenclick;
    public GameObject imagenclicktabla;
    public GameObject informacionfrascocanvas;
    public Text aviso;
    public Text avisoClick;
    //string nombre;
    //string nombreFrasco;
    //int comprobante;

    void start()
    {
        //funcion = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<RayCastScript>();

        texto.SetActive(false);
        titulo.SetActive(false);
        camaraDescripcion.SetActive(false);
        camaraJuego.SetActive(false);
        cartelAviso.SetActive(false);
        informacionFrasco.SetActive(false);
        click.SetActive(false);
        imagenclick.SetActive(false);
        imagenclicktabla.SetActive(false);

        jugador.SetActive(true);
        papa_frita.enabled = false;
        mov.SetActive(true);

        //funcion.comprobante = 0;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            texto.SetActive(false);
            titulo.SetActive(false);
            cartelAviso.SetActive(false);
            camaraDescripcion.SetActive(false);
            informacionFrasco.SetActive(false);
            camaraJuego.SetActive(false);
            cursor.SetActive(true);
            click.SetActive(false);
            imagenclicktabla.SetActive(false);

            //funcion.comprobante = 0;

            jugador.SetActive(true);

            mov.SetActive(true);
            papa_frita.enabled = false;
            mov.transform.position = camarajugador.transform.position;

        }

    }


}