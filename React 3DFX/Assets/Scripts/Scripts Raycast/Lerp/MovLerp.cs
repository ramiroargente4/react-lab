﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovLerp : MonoBehaviour
{


    public GameObject consigna;

    public GameObject canvastabla;
    public Camera pantallaCarga;
    public GameObject pantallaInfo;

    public GameObject jugador;
    public GameObject cameradescripcion;
    public GameObject camaramov;
    public Camera mov;
    public GameObject go1;
    public GameObject go2;
    public bool lerp;
    public float speed;
    public CargarPorTiempoEscena carga;

    void Start()
    {
        //cambio = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<RayCastScript>();
        //info = GameObject.FindGameObjectWithTag("TablaPeriodica").GetComponent<ElementalInformation>();
        lerp = true;
        carga = GameObject.FindGameObjectWithTag("PantallaCarga").GetComponent<CargarPorTiempoEscena>();
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Libro")
        {
            //cameradescripcion.SetActive(true);
            //camaramov.SetActive(false);
            mov.enabled = false;
            lerp = false;
            consigna.SetActive(false);

            //carga.CambiarCamara();
            pantallaCarga.enabled = false;
            pantallaInfo.SetActive(true);
            Debug.Log("Cambia la cámara");
            canvastabla.SetActive(true);
            

            //cambio.nombre = info.nombre;
            //info.giveInformation();

            Debug.Log("colisiono");
        }
    }

    void Update()
    {
        //RaycastHit hit;
        //if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        //if (hit.transform.tag == "TablaPeriodica")
        //        //{
        //            lerp = true;
        //        //}
        //    }
        //    //lerp = true;
        //    //else 
        //    //{
        //    //    lerp = false;
        //    //}
        //}
        if (lerp)
        {
            if (Vector3.Distance(transform.position, go2.transform.position) > 0.01f)
            {
                //jugador.SetActive(false);
                transform.position = Vector3.Lerp(transform.position, go1.transform.position, Time.deltaTime * speed);
                transform.rotation = Quaternion.Lerp(transform.rotation, go2.transform.rotation, Time.deltaTime * speed);
                transform.position = Vector3.Lerp(transform.position, go2.transform.position, Time.deltaTime * speed);
            }

            //else
            //    lerp = false;
        }
        else
        {
            jugador.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            lerp = true;
        }
    }



}