﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RayCastScript : MonoBehaviour
{


    public GameObject consigna;

    bool mariano; 
    mision_elementos comprobador_interruptor;

    //UI y funcionamiento
    public GameObject texto;
    public GameObject titulo;
    public GameObject camaraJuego;
    public GameObject camaraDescripcion;
    public GameObject cartelAviso;
    public GameObject cursor;
    public GameObject informacionFrasco;
    public GameObject click;
    public GameObject imagenclick;
    public GameObject imagenclicktabla;
    public GameObject informacionfrascocanvas;
    public GameObject canvastabla;
    public GameObject imagenP;

    //luces, invernadero, tablas
    public GameObject parasalir;
    public GameObject tablacomun;
    public GameObject tablaextendida;
    public GameObject invernadero;
    public GameObject luces;
    public GameObject tablagrande;
    public GameObject tablaresumida;

    //interruptores
    public GameObject interruptorlucesapagar;
    public GameObject interruptorlucesprender;
    public GameObject interruptortablaapagar;
    public GameObject interruptortablaprender;
    public GameObject interruptorescenariotabla1;
    public GameObject interruptorescenariotabla0;
    //rotacion tabla periodica
    public GameObject divisortablaperiodica;
    public GameObject tablaperiodicaelementos;

    public GameObject elementosNoPosee;
    public GameObject elementosAlcalino;
    public GameObject elementosAlcalinoTerreos;
    public GameObject elementosLantanidos;
    public GameObject elementosActinido;
    public GameObject elementosMetalTransicion;
    public GameObject elementosMetalBloqueP;
    public GameObject elementosSemimetal;
    public GameObject elementosOtrosNoMetales;
    public GameObject elementosGasesNobles;

    public GameObject textoAlcalino;
    public GameObject textoAlcalinoTerreos;
    public GameObject textoLantanidos;
    public GameObject textoActinido;
    public GameObject textoMetalTransicion;
    public GameObject textoMetalBloqueP;
    public GameObject textoSemimetal;
    public GameObject textoOtrosNoMetales;
    public GameObject textoGasesNobles;
    //Cambio colores
    public Material liquidofrasco;






    public Text aviso;
    public Text avisoClick;
    string nombre;
    string nombreFrasco;
    ElementalInformation funcion;
    public int comprobante;
    [SerializeField] private Animator anim;
    private bool luzprendida = false;
    private bool modostabla = false;
    private bool modotablaescenario = false;
    public GameObject jugador;
    public GameObject mov;
    public GameObject camarajugador;
    public Camera cameraComponent;

    public MovLerp trato;

    public RayCastScript ray;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController movimiento;

    

    public void seleccion_elemento_mision(RaycastHit hit)
    {
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "Elemento mision")
                {   
                texto.SetActive(true);
                titulo.SetActive(true);
                consigna.SetActive(false);

                //camaraDescripcion.SetActive(true);

                cartelAviso.SetActive(false);
                camaraJuego.SetActive(false);
                cursor.SetActive(false);
                click.SetActive(false);
                imagenclicktabla.SetActive(false);
                comprobante = 1;

                //trato.enabled = true;

                //jugador.SetActive(false);
                trato.enabled = true;
                cameraComponent.enabled = true;
                movimiento.enabled = false;
                ray.enabled = false;
                }
    }


    public void seleccion_objeto(RaycastHit hit)
    {
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "Elementos de laboratorio")
            {
                ElementalInformation test = hit.collider.gameObject.GetComponent<ElementalInformation>();
                cursor.SetActive(false);
                informacionFrasco.SetActive(true);
                click.SetActive(false);
                imagenclick.SetActive(false);
                cartelAviso.SetActive(true);
                imagenP.SetActive(true);
                parasalir.SetActive(true);
                test.dameLaInfo();
                comprobante = 1;

            }
            else if (hit.transform.tag == "TablaPeriodica")
            {
                texto.SetActive(true);
                titulo.SetActive(true);
                consigna.SetActive(false);

                //camaraDescripcion.SetActive(true);

                cartelAviso.SetActive(false);
                camaraJuego.SetActive(false);
                cursor.SetActive(false);
                click.SetActive(false);
                imagenclicktabla.SetActive(false);
                comprobante = 1;

                //trato.enabled = true;

                //jugador.SetActive(false);
                trato.enabled = true;
                cameraComponent.enabled = true;
                movimiento.enabled = false;
                ray.enabled = false;
            }
    }

    public void reconocimiento_objeto(RaycastHit hit)
    {
        if (hit.transform.tag == "invernadero")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Abrir invernadero";
        }

        else if (hit.transform.tag == "invernadero 2")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Cerrar invernadero";
        }

        else if (hit.transform.tag == "Elementos de laboratorio" || hit.transform.tag == "Consigna 1" || hit.transform.tag == "Consigna 2" || hit.transform.tag == "Consigna 3")
        {
            ElementalInformation test = hit.collider.gameObject.GetComponent<ElementalInformation>();
            nombreFrasco = test.frascoNombre;
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = nombreFrasco;
        }
        else if (hit.transform.tag == "TablaPeriodica")
        {
            ElementalInformation test = hit.collider.gameObject.GetComponent<ElementalInformation>();
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            informacionFrasco.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            nombre = test.nombre;
            aviso.text = nombre;
            avisoClick.text = nombre;
            test.giveInformation();
        }
        else
        {
            click.SetActive(false);
            imagenclick.SetActive(false);
            imagenclicktabla.SetActive(false);
            comprobante = 0;
        }
    }

    public void salir(RaycastHit hit)
    {
        if (hit.transform.tag == "puertasalir")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Volver al menu";
        }
    }

    public void interaccioninterruptores(RaycastHit hit)
    {
        if (hit.transform.tag == "tablaextendida")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Modos de la tabla";
        }
        // ESTOY HACIENDO LO DE APAGAR LAS LUCES EN LA FUNCION DE LA TABLA PORQUE SI NO NO FUNCA
        if (hit.transform.tag == "lucesprender")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Alternar luces";
        }
        if (hit.transform.tag == "tablaexplicada")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Serie/Completa";
        }
        if (hit.transform.tag == "EscenarioTabla")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Ir al escenario";
        }
        if (hit.transform.tag == "volverAlab")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Volver al lab";
        }
        if (hit.transform.tag == "IrInvernadero")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Ir al invernadero";
        }
        if (hit.transform.tag == "IrLabViejo")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Ir al lab de 2021";
        }
    }

    public void interaccionrotaciontablaperiodica(RaycastHit hit)
    {
        if (hit.transform.tag == "etAlcalino")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Alcalino";
        }
        if (hit.transform.tag == "etAlcalinoterreos")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Alcalinoterreos";
        }
        if (hit.transform.tag == "etLantanidos")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Lantanidos";
        }
        if (hit.transform.tag == "etActinido")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Actinidos";
        }
        if (hit.transform.tag == "etMetaldetransicion")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Metales transicion";
        }
        if (hit.transform.tag == "etMetalbloquep")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Metales bloque p";
        }
        if (hit.transform.tag == "etMetalbloquep")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Metales bloque p";
        }
        if (hit.transform.tag == "etSemimetal")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Semimetales";
        }
        if (hit.transform.tag == "etOtrosnometales")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Otros no metales";
        }
        if (hit.transform.tag == "etGasesnobles")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Gases nobles";
        }
        if (hit.transform.tag == "etMetal")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Metales";
        }
        if (hit.transform.tag == "etNometal")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Mostrar Nometales";
        }
        if (hit.transform.tag == "etReiniciarTabla")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Reiniciar tabla";
        }
    }

    public void interruptoreslucestablas(RaycastHit hit)
    {

        //comprobador_interruptor = FindObjectOfType<mision_elementos>();
        //mariano = comprobador_interruptor.elementos_terminados;
        //funciona = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<mision_elementos>();
        //mariano = funciona.elementos_terminados;

        //mision_elementos hola;
        //mariano = hola.elementos_terminados;

        //if (Input.GetMouseButtonDown(0))
        //if (hit.transform.tag == "invernadero")
        //{
        //  anim.SetBool("subirpared", true);
        //  anim.SetBool("bajarpared", false);
        //   invernadero.SetActive(true);
        //}
        //if (Input.GetMouseButtonDown(0))
        // if (hit.transform.tag == "invernadero 2")
        // {
        //  anim.SetBool("subirpared", false);
        //  anim.SetBool("bajarpared", true);
        //  invernadero.SetActive(false);
        //yield return new WaitForSeconds(2f);

        // }

        //anim.SetBool("subirpared", false);
        //anim.SetBool("bajarpared", false);

        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "tablaextendida" && modostabla == false)
            {
                interruptortablaprender.SetActive(false);
                interruptortablaapagar.SetActive(true);
                tablacomun.SetActive(false);
                tablaextendida.SetActive(true);
                modostabla = true;
            }
            else if (hit.transform.tag == "tablaextendida" && modostabla == true)
            {
                interruptortablaprender.SetActive(true);
                interruptortablaapagar.SetActive(false);
                tablacomun.SetActive(true);
                tablaextendida.SetActive(false);
                modostabla = false;
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "puertasalir")
            {
                SceneManager.LoadScene("carga pantalla 3");
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "lucesprender" && luzprendida == false)
            {
                luces.SetActive(true);
                interruptorlucesprender.SetActive(false);
                interruptorlucesapagar.SetActive(true);
                luzprendida = true;
            }
            else if (hit.transform.tag == "lucesprender" && luzprendida == true)
            {
                luces.SetActive(false);
                interruptorlucesprender.SetActive(true);
                interruptorlucesapagar.SetActive(false);
                luzprendida = false;

            }
        if (Input.GetMouseButtonDown(0))
        
            if (hit.transform.tag == "tablaexplicada" && modotablaescenario == false)
            {
                interruptorescenariotabla1.SetActive(false);
                interruptorescenariotabla0.SetActive(true);
                tablagrande.SetActive(false);
                tablaresumida.SetActive(true);
                modotablaescenario = true;
            }
            else if (hit.transform.tag == "tablaexplicada" && modotablaescenario == true)
            {
                interruptorescenariotabla1.SetActive(true);
                interruptorescenariotabla0.SetActive(false);
                tablagrande.SetActive(true);
                tablaresumida.SetActive(false);
                modotablaescenario = false;
            }
    }

    public void rotaciontabla(RaycastHit hit)
    {
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etAlcalino")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosAlcalino.SetActive(true);
                textoAlcalino.SetActive(true);

                elementosNoPosee.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosSemimetal.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);
                elementosGasesNobles.SetActive(false);

                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoSemimetal.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etAlcalinoterreos")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosAlcalinoTerreos.SetActive(true);
                textoAlcalinoTerreos.SetActive(true);

                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosSemimetal.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);
                elementosGasesNobles.SetActive(false);

                textoAlcalino.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoSemimetal.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etLantanidos")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosLantanidos.SetActive(true);
                textoLantanidos.SetActive(true);



                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosSemimetal.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);
                elementosGasesNobles.SetActive(false);

                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoSemimetal.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etActinido")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosActinido.SetActive(true);
                textoActinido.SetActive(true);


                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosSemimetal.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);
                elementosGasesNobles.SetActive(false);

                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoSemimetal.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etMetaldetransicion")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosMetalTransicion.SetActive(true);
                textoMetalTransicion.SetActive(true);


                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosSemimetal.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);
                elementosGasesNobles.SetActive(false);

                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoSemimetal.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etMetalbloquep")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosMetalBloqueP.SetActive(true);
                textoMetalBloqueP.SetActive(true);


                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosSemimetal.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);
                elementosGasesNobles.SetActive(false);

                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoSemimetal.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etSemimetal")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosSemimetal.SetActive(true);
                textoSemimetal.SetActive(true);

                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);
                elementosGasesNobles.SetActive(false);

                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etOtrosnometales")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosOtrosNoMetales.SetActive(true);
                textoOtrosNoMetales.SetActive(true);


                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosSemimetal.SetActive(false);
                elementosGasesNobles.SetActive(false);

                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoSemimetal.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etGasesnobles")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);
                elementosGasesNobles.SetActive(true);
                textoGasesNobles.SetActive(true);

                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosSemimetal.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);

                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoSemimetal.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etMetal")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);

                elementosAlcalino.SetActive(true);
                elementosAlcalinoTerreos.SetActive(true);
                elementosLantanidos.SetActive(true);
                elementosActinido.SetActive(true);
                elementosMetalTransicion.SetActive(true);
                elementosMetalBloqueP.SetActive(true);
                textoAlcalino.SetActive(true);
                textoAlcalinoTerreos.SetActive(true);
                textoLantanidos.SetActive(true);
                textoActinido.SetActive(true);
                textoMetalTransicion.SetActive(true);
                textoMetalBloqueP.SetActive(true);

                elementosNoPosee.SetActive(false);             
                elementosSemimetal.SetActive(false);
                elementosOtrosNoMetales.SetActive(false);
                elementosGasesNobles.SetActive(false);
                
                textoSemimetal.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etNometal")
            {
                divisortablaperiodica.SetActive(false);
                tablaperiodicaelementos.SetActive(false);

                elementosOtrosNoMetales.SetActive(true);
                elementosGasesNobles.SetActive(true);
                textoOtrosNoMetales.SetActive(true);
                textoGasesNobles.SetActive(true);

                elementosNoPosee.SetActive(false);
                elementosAlcalino.SetActive(false);
                elementosAlcalinoTerreos.SetActive(false);
                elementosLantanidos.SetActive(false);
                elementosActinido.SetActive(false);
                elementosMetalTransicion.SetActive(false);
                elementosMetalBloqueP.SetActive(false);
                elementosSemimetal.SetActive(false);


                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoSemimetal.SetActive(false);
            }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "etReiniciarTabla")
            {
                divisortablaperiodica.SetActive(true);
                tablaperiodicaelementos.SetActive(true);
                elementosOtrosNoMetales.SetActive(true);
                elementosGasesNobles.SetActive(true);
                elementosNoPosee.SetActive(true);
                elementosAlcalino.SetActive(true);
                elementosAlcalinoTerreos.SetActive(true);
                elementosLantanidos.SetActive(true);
                elementosActinido.SetActive(true);
                elementosMetalTransicion.SetActive(true);
                elementosMetalBloqueP.SetActive(true);
                elementosSemimetal.SetActive(true);


                textoAlcalino.SetActive(false);
                textoAlcalinoTerreos.SetActive(false);
                textoLantanidos.SetActive(false);
                textoActinido.SetActive(false);
                textoMetalTransicion.SetActive(false);
                textoMetalBloqueP.SetActive(false);
                textoOtrosNoMetales.SetActive(false);
                textoGasesNobles.SetActive(false);
                textoSemimetal.SetActive(false);
            }
    }

    public void interaccionescenario(RaycastHit hit)
    {
        if (hit.transform.tag == "Nombre del elemento")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Nombre del elemento";
        }
        if (hit.transform.tag == "Electronegatividad")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Electronegatividad del elemento";
        }
        if (hit.transform.tag == "Peso atomico")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Peso atomico del elemento";
        }
        if (hit.transform.tag == "Electrones de valencia")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Electrones de valencia del elemento";
        }
        if (hit.transform.tag == "Simbolo")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Simbolo del elemento";
        }
        if (hit.transform.tag == "Numero atomico")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Numero atomico del elemento";
        }
    }

    public void interaccionanimacionescalentamiento(RaycastHit hit)
    {
        if (hit.transform.tag == "Iniciar#1")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Iniciar c/tripode";
        }
        if (hit.transform.tag == "Iniciar#2")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Iniciar c/crisol";
        }
        if (hit.transform.tag == "iniciarSodio")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Iniciar";
        }

    }

    public void visorvideosreacciones(RaycastHit hit)
    {
        if (hit.transform.tag == "VideoReaccion")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Ver video";
        }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "VideoReaccion")
            {
                SceneManager.LoadScene("videoreaccion");
            }
    }
    public void accederalvisor(RaycastHit hit)
    {
        if (hit.transform.tag == "accedervisor")
        {
            imagenP.SetActive(false);
            parasalir.SetActive(false);
            click.SetActive(true);
            imagenclick.SetActive(true);
            cartelAviso.SetActive(true);
            imagenclicktabla.SetActive(false);
            avisoClick.text = "Ir";
        }
        if (Input.GetMouseButtonDown(0))
            if (hit.transform.tag == "accedervisor")
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                SceneManager.LoadScene("visor");
            }
    }

    public void principal()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
        {
            reconocimiento_objeto(hit);

            interaccioninterruptores(hit);

            salir(hit);

            seleccion_objeto(hit);

            //interruptor_tablaperidioca(hit);

            interruptoreslucestablas(hit);

            rotaciontabla(hit);

            interaccionrotaciontablaperiodica(hit);

            interaccionescenario(hit);

            interaccionanimacionescalentamiento(hit);

            visorvideosreacciones(hit);

            accederalvisor(hit);
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        

        imagenP.SetActive(false);
        parasalir.SetActive(false);


        texto.SetActive(false);
        titulo.SetActive(false);
        camaraDescripcion.SetActive(false);
        camaraJuego.SetActive(false);
        cartelAviso.SetActive(false);
        informacionFrasco.SetActive(false);
        click.SetActive(false);
        imagenclick.SetActive(false);
        imagenclicktabla.SetActive(false);

        //invernadero.SetActive(false);

        jugador.SetActive(true);

        comprobante = 0;

        funcion = GameObject.FindGameObjectWithTag("TablaPeriodica").GetComponent<ElementalInformation>();


        cameraComponent.enabled = false;
        trato.enabled = false;
        mov.SetActive(true);
        liquidofrasco.color = Color.blue;
    }

    // Update is called once per frame
    //Physics.Raycast(Origin, Direction, hit, Distance, LayerMask, queryTrigger)
    void LateUpdate()
    {
        if (comprobante == 0)
        {

            principal();

        }

        if (Input.GetKeyDown(KeyCode.P))
        {

            //GameObject.Find("MainCamera").GetComponent<RayCastScript>().enabled = true;


            consigna.SetActive(true);
            texto.SetActive(false);
            titulo.SetActive(false);
            cartelAviso.SetActive(false);
            camaraDescripcion.SetActive(false);
            informacionFrasco.SetActive(false);
            camaraJuego.SetActive(false);
            cursor.SetActive(true);
            click.SetActive(false);
            imagenclicktabla.SetActive(false);
            comprobante = 0;


            trato.enabled = false;
            jugador.SetActive(true);

            movimiento.enabled = true;
            ray.enabled = true;

            cameraComponent.transform.position = camarajugador.transform.position;
            cameraComponent.transform.rotation = camaraDescripcion.transform.rotation;
            cameraComponent.transform.Rotate(new Vector3(0, 90, 0));
        }

    }

}
