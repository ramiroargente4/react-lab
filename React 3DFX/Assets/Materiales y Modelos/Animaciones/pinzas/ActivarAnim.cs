﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//EXPLICACION

//El script esencialmente tiene una sola funcion, activar el animator de los objetos. Teniendo en cuenta que 
//no hay loop time, y se ejecutan una vez al activar el animator, es la manera mas facil de lograrlo.
//Una vez activado el booleano, las tres animaciones van a arrancar
//El bunsen por ejemplo, se anima de 00 a 4, mientras que la tela va de 4:30 a 9 por ej, y asi.
public class ActivarAnim : MonoBehaviour
{
    public bool activartodo1;
    public bool activartodo2;

    public Animator animBunsen;
    public Animator animTela;
    public Animator animMatraz;

    public Animator animBunsen2;
    public Animator animTriangulo;
    public Animator animPinzas;

    // Start is called before the first frame update



    void Start()
    {

        //animBunsen = GetComponent<Animator>();
        //animTela = GetComponent<Animator>();
        //animMatraz = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 10f))
        {
            if (Input.GetMouseButtonDown(0)) 
             
                if (hit.transform.tag == "Iniciar#1")
                {
                    activartodo1 = true;
                }
            
            if (Input.GetMouseButtonDown(0))

            if (hit.transform.tag == "Iniciar#2")
            {
                activartodo2 = true;
            }
        }

        if (activartodo1 == true)
        {
            animBunsen.enabled = !animBunsen.enabled;
            animTela.enabled = !animTela.enabled;
            animMatraz.enabled = !animMatraz.enabled;
            activartodo1 = false;
        }
        if (activartodo2 == true)
        {
            animBunsen2.enabled = !animBunsen2.enabled;
            animTriangulo.enabled = !animTriangulo.enabled;
            animPinzas.enabled = !animPinzas.enabled;
            activartodo2 = false;
        }
        


    }
}
