﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PlayVideo2 : MonoBehaviour
{

    public RawImage menu;
    public VideoPlayer video;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(Playvideo());

    }
    IEnumerator Playvideo()
    {

        video.Prepare();
        WaitForSeconds segundos = new WaitForSeconds(1);
        while (!video.isPrepared)
        {
            yield return segundos;
            break;
        }

        menu.texture = video.texture;
        video.Play();

    }



    // Update is called once per frame
    void Update() { }


}
   


