﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;



public class AdminEscenas : MonoBehaviour
{

    // Objetos publicos del menu
    public GameObject quienessomos;
    public GameObject menuprincipal;
    public GameObject configuracion;
    public GameObject postiniciar;
    public GameObject seleccionmisiones;
    public GameObject controles;
    public GameObject tutorial;
    //public GameObject videotutorial;
    //public VideoPlayer videotutorial;
    //public GameObject playscript;


    // Start is called before the first frame update
    void Start()
    {
        //videotutorial.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CargarJuego()
    {

        SceneManager.LoadScene("Lab");
    }
    public void PantallaCarga()
    {

        SceneManager.LoadScene("carga pantalla");
    }
    public void CargarMisionTabla()
    {
        SceneManager.LoadScene("cargamision");
    }
    public void ActivarPostIniciar()
    {
        postiniciar.SetActive(true);
        menuprincipal.SetActive(false);
        seleccionmisiones.SetActive(false);
    }
    public void ActivarSeleccionMisiones()
    {
        seleccionmisiones.SetActive(true);
        postiniciar.SetActive(false);

    }
    public void ActivarConfiguracion()
    {
        menuprincipal.SetActive(false);
        configuracion.SetActive(true);
        controles.SetActive(false);
    }
    public void ActivarQuienesSomos()
    {
        menuprincipal.SetActive(false);
        quienessomos.SetActive(true);

      //SceneManager.LoadScene("carga pantalla menu 2");
    }
    public void ActivarMenu()
    {
        menuprincipal.SetActive(true);
        quienessomos.SetActive(false);
        configuracion.SetActive(false);
        postiniciar.SetActive(false);
        tutorial.SetActive(false);
        // SceneManager.LoadScene("carga pantalla 3");
    }
    public void ActivarControles()
    {
        controles.SetActive(true);
        configuracion.SetActive(false);
        // SceneManager.LoadScene("carga pantalla 3");
    }
    public void ActivarTutorial()
    {
        SceneManager.LoadScene("cargatuto"); 
    }
}

