﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CargarTiempo : MonoBehaviour
{
    [SerializeField]
    private float tardarantesdecargar = 10f;
    [SerializeField]
    private string escenaacargar;
    private float tiempopasado;
    private void Update()

    {
        tiempopasado += Time.deltaTime;

        if (tiempopasado > tardarantesdecargar)
        {
            SceneManager.LoadScene(escenaacargar);
        }
    }
}
