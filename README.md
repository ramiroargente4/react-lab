# React Lab

## Description

A simulator that recreates the experience on a real Laboratory. Main features include 3D object viewers, different flasks and an interactive periodic table.

## Installation

Use latest 2019 Unity version to load the files. Remember that it will take some time as the "library" folder is git-ignored


